package com.gl.service;
import com.gl.dao.*;
import com.gl.pojo.Admin;
import com.gl.pojo.Book;
import java.util.*;
import java.util.List;
public class AdminServiceImpl implements AdminService {

	private Scanner sc;
	AdminDAO admindao;//declaration of Admin dao class
	public AdminServiceImpl()
	{
		sc=new Scanner(System.in);
		admindao=new AdminDAOImpl();//initializing of Admin dao class
		
	}
	@Override
	//Admin registration
	public void register() {
		System.out.println("Enter how many Admins u want to add");
		int noofusers=sc.nextInt();
		for(int x=1;x<=noofusers;x++)
		{
		Admin user=new Admin();
		System.out.println("Enter Userid");
		user.setUserid(sc.nextInt());
		System.out.println("Enter User Name");
		user.setUsername(sc.next());
		System.out.println("Enter Password ");
		user.setPassword(sc.next());
		admindao.register(user);
		}
		
	}
	//Admin login
	@Override
	public void login() {
		Admin checkAdmin=new Admin();
		System.out.println("Enter Userid");
		checkAdmin.setUserid(sc.nextInt());
		System.out.println("Enter user name ");
		checkAdmin.setUsername(sc.next());
		System.out.println("Enter PAssword ");
		checkAdmin.setPassword(sc.next());
		admindao.login(checkAdmin);
		
	}
	@Override
	//adding new books in a book list
	public void addBook() {
		System.out.println("Enter no of books u want to add ");
		int noofbooks=sc.nextInt();
		Book book=new Book();
		for(int x=1;x<=noofbooks;x++)
		{
			System.out.println("Enter Bookid");
			book.setBookId(sc.nextInt());
			System.out.println("Enter Bookname ");
			book.setBookName(sc.next());
			System.out.println("Enter AuthorName ");
			book.setAuthorname(sc.next());
			System.out.println("Enter Descripstion ");
			book.setDescription(sc.next());
			System.out.println("Enter Genre ");
			book.setGenre(sc.next());
			System.out.println("Enter NoOfCopiesSold ");
			book.setNoOfCopiesSold(sc.nextInt());
			System.out.println("Enter Prize ");
			book.setPrice(sc.nextDouble());
			admindao.addBook(book);
			
		}
	}
	//deleting  books in a book list
	@Override
	public void deleteBook() {
		System.out.println("Enter Bookid which u want to delete");
		int bookid=sc.nextInt();
		Book deletebook=new Book();
		deletebook.setBookId(bookid);
		admindao.deleteBook(deletebook);
		
	}
	//updating  books in a book list
	@Override
	public void updateBook() {
		System.out.println("Enter BookId which u want to update");
		int bookid=sc.nextInt();
		Book updatebook=new Book();
		updatebook.setBookId(bookid);
		System.out.println("Enter new Bookname ");
		updatebook.setBookName(sc.next());
		System.out.println("Enter new AuthorName ");
		updatebook.setAuthorname(sc.next());
		System.out.println("Enter new Descripstion ");
		updatebook.setDescription(sc.next());
		System.out.println("Enter new Genre ");
		updatebook.setGenre(sc.next());
		System.out.println("Enter new NoOfCopiesSold ");
		updatebook.setNoOfCopiesSold(sc.nextInt());
		System.out.println("Enter new Prize ");
		updatebook.setPrice(sc.nextDouble());
		admindao.updateBook(updatebook);
		
		
		
	}
	//dispalying All books 
	@Override
	public void displayBook() {
		List<Book> booklist=admindao.display();
		for(Book b:booklist)
		{
			System.out.println("BookId is     	  :"+b.getBookId());
			System.out.println("BookName is   	  :"+b.getBookName());
			System.out.println("AuthorName is	  :"+b.getAuthorname());
			System.out.println("Descripstion  	  :"+b.getDescription());
			System.out.println("Genre is          :"+b.getGenre());
			System.out.println("NoOfCopiesSold is :"+b.getNoOfCopiesSold());
			System.out.println("Prize is          :"+b.getPrice());
			System.out.println(".....................................................");
		}
		
		
	}
	//count of total no of books in a book list 
	@Override
	public void totalCountOfBooks() {
		System.out.println("count of all books");
		List<Book> booklist= admindao.totalCountOfBooks();
		booklist.forEach(
				e->
				{
					System.out.println("BookName   :"+e.getBookName());
				}
						);
	}
	//list of Autobiography genre
	@Override
	public List<Book> autoBioGenre(){
		System.out.println("List of all the books under Autobiography genre");
		List<Book> booklist=admindao.Autobiography_genre();
		booklist.forEach(
				e->
				{
					System.out.println("BookName   :"+e.getBookName());
				}
						);
		return booklist;
	}
	//Ascending order according to price
	@Override
	public void lowToHighPrice() {
		System.out.println("Order of books in ascending order according to price");
		List<Book> booklist=admindao.lowToHigh_Price();
		Comparator<Book> sortlowToHighPrice=(book1,book2)->
		{
			return (book1.getPrice()).compareTo(book2.getPrice());
		};
		Collections.sort(booklist,sortlowToHighPrice);
		booklist.forEach(
				e->
				{
					System.out.println("BookName   :"+e.getBookName());
				}
						);
		
		
		
	}
	//Descending order according to price
	@Override
	public void highToLowPrice() {
		System.out.println("Order of books in descending order according to price");
		List<Book> booklist=admindao.highToLow_Price();
		Comparator<Book> sortHighToLowPrice=(book1,book2)->
		{
			return (book1.getPrice()).compareTo(book2.getPrice());
		};
		Collections.sort(booklist,sortHighToLowPrice.reversed());
		booklist.forEach(
				e->
				{
					System.out.println("BookName   :"+e.getBookName());
				}
						);
	}
	@Override
	//list of best selling books
	public void bestselling() {
		System.out.println("Order of books  according to NoOfCopiesSold ");
		List<Book> booklist=admindao.bestselling();
		Comparator<Book> sortingByNoOfCopiesSold=(book1,book2)->
		{
			return(book1.getNoOfCopiesSold()).compareTo(book2.getNoOfCopiesSold());
		};
		Collections.sort(booklist,sortingByNoOfCopiesSold.reversed());
		booklist.forEach(
				e->
				{
					System.out.println("BookName   : "+e.getBookName());
				}
				);
		
	}

	
}










