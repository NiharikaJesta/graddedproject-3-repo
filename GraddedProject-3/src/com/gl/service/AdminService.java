package com.gl.service;

import java.util.List;

import com.gl.pojo.Book;

public interface AdminService {
	//Admin interface method declarations
	public void register();
	public void login();
	public void addBook();
	public void deleteBook();
	public void updateBook();
	public void displayBook();
	public void totalCountOfBooks();
	public List<Book> autoBioGenre();
	public void lowToHighPrice();
	public void highToLowPrice();
	public void bestselling();
	
}
