package com.gl.service;
import com.gl.exception.*;


public interface UserService {
	//user interface method declarations
	public void register();
	public void login();
	public void selectbookid()throws NegitiveException;
	public void newbooks();
	public void favbooks();
	public void completedbooks();
	public void viewallbooks();
}
