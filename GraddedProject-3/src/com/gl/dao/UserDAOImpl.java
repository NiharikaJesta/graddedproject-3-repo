package com.gl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import com.gl.connector.DataConnector;
import com.gl.pojo.Book;
import com.gl.pojo.User;

public class UserDAOImpl implements UserDAO{
	private Connection con1;
	private PreparedStatement stat;
	private Scanner sc;
	public UserDAOImpl()
	{
		sc=new Scanner(System.in);
		con1=DataConnector.getConnect();
	}
	
	@Override
	public void register(User u)  {
			try {
				stat=con1.prepareStatement("insert into user values(?,?,?,?)");
				stat.setString(1, u.getUsername());
				stat.setInt(2, u.getUserid());
				stat.setString(3, u.getEmailid());
				stat.setString(4, u.getPassword());
				
				int result=stat.executeUpdate();
				if(result>0)
				{
					System.out.println("Registartion done sucessfully ");
				}
				}
				catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}
	}

	@Override
	public boolean login(User u) {
		try {
			stat=con1.prepareStatement("select* from user where username=? and userid=? and emailid=? and password=?");
			               
			stat.setString(1,u.getUsername());
			stat.setInt(2,u.getUserid());
			stat.setString(3, u.getEmailid());
			stat.setString(4,u.getPassword());
			ResultSet result= stat.executeQuery();
			if(result.next())
			{
				System.out.println("Login successfully");
				return true;
			}
			else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
		
	}

}
