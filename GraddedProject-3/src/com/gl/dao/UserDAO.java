package com.gl.dao;

import java.util.List;

import com.gl.pojo.*;

public interface UserDAO {
	public void register(User u);
	public boolean login(User u);
	
}
