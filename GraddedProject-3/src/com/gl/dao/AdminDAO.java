package com.gl.dao;

import java.util.List;

import com.gl.pojo.Admin;
import com.gl.pojo.Book;

public interface AdminDAO {
	public void register(Admin a);
	public boolean login(Admin a);
	public void addBook(Book b);
	public void deleteBook(Book bookId);
	public void updateBook(Book bookId);
	public List<Book> display();
	public List<Book> totalCountOfBooks();
	public List<Book>  Autobiography_genre();
	public List<Book> lowToHigh_Price();
    public List<Book> highToLow_Price();
    public List<Book> bestselling();
}
