package com.gl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.gl.connector.DataConnector;
import com.gl.pojo.Admin;
import com.gl.pojo.Book;


public class AdminDAOImpl implements AdminDAO{
	//this class is used to write queries to connect the database (java_sql)
	private Connection con1;
	private PreparedStatement stat;
	private Scanner sc;
	public AdminDAOImpl()
	{
		sc=new Scanner(System.in);
		con1=DataConnector.getConnect();
	}
	@Override
	public void register(Admin a) {
		try {
			stat=con1.prepareStatement("insert into admin values(?,?,?)");
			stat.setInt(1, a.getUserid());
			stat.setString(2, a.getUsername());
			stat.setString(3, a.getPassword());
			int result=stat.executeUpdate();
			if(result>0)
			{
				System.out.println("Registartion done sucessfully");
			}
			}
			catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
		
	}

	@Override
	public boolean login(Admin a) {
		try
		{
			stat=con1.prepareStatement("select * from admin where userid = ? and username=? and password = ? ");
			stat.setInt(1,a.getUserid());               
			stat.setString(2,a.getUsername());
			stat.setString(3,a.getPassword());
			ResultSet result= stat.executeQuery();
			if(result.next())
			{
				System.out.println("login successfully");
				return true;
			}
			else
			{
				System.out.println("login Failed");
				return false;
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
		
	}
	@Override
	public void addBook(Book b) {
		try {
			stat=con1.prepareStatement("insert into Book values(?,?,?,?,?,?,?)");
			stat.setInt(1, b.getBookId());
			stat.setString(2,b.getBookName());
			stat.setString(3, b.getAuthorname());
			stat.setString(4, b.getDescription());
			stat.setString(5, b.getGenre());
			stat.setInt(6, b.getNoOfCopiesSold());
			stat.setDouble(7, b.getPrice());
			int result=stat.executeUpdate();
			if(result>0)
			{
				System.out.println("Book Data inserted");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	@Override
	public void deleteBook(Book bookId) {
		try
		{
			stat=con1.prepareStatement("delete from Book where bookid=?");
			stat.setInt(1, bookId.getBookId());
			int result=stat.executeUpdate();
			if(result>0)
			{
				System.out.println("Book Data deleted success");
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		
	}
	@Override
	public void updateBook(Book bookId) {
		try
		{
		stat=con1.prepareStatement("update book set bookName=? , authorname=? , description=? , genre=? , noofcopiessold=? , price=? where bookid=?");
		stat.setString(1, bookId.getBookName());
		stat.setString(2, bookId.getAuthorname());
		stat.setString(3, bookId.getDescription());
		stat.setString(4, bookId.getGenre());
		stat.setInt(5, bookId.getNoOfCopiesSold());
		stat.setDouble(6, bookId.getPrice());
		stat.setInt(7, bookId.getBookId());

		int result=stat.executeUpdate();
		if(result>0)
		{
			System.out.println("Book Data updated success");
		}
		else
		{
			System.out.println("Book Data not updated");
		}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		
		
	}
	@Override
	public List<Book> display() {
		List<Book> booklist=new ArrayList<Book>();
		try {
			stat=con1.prepareStatement("select * from Book");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Book b=new Book();
				b.setBookId(result.getInt(1));
				b.setBookName(result.getString(2));
				b.setAuthorname(result.getString(3));
				b.setDescription(result.getString(4));
				b.setGenre(result.getString(5));
				b.setNoOfCopiesSold(result.getInt(6));
				b.setPrice(result.getDouble(7));

				booklist.add(b);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return booklist;
		
	}
	@Override
	public List<Book>totalCountOfBooks() {
		List<Book> bookcount=new ArrayList<Book>();
		try {
			stat=con1.prepareStatement("select count(*) from book  ");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Book b=new Book();
				b.setBookName(result.getString(1));
				bookcount.add(b);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bookcount;
		
	}
	@Override
	public List<Book> Autobiography_genre() {
		List<Book> booklist=new ArrayList<Book>();
		try {
			stat=con1.prepareStatement("select * from book where genre='autobiography'");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Book b=new Book();
				b.setBookName(result.getString(2));
				b.setGenre(result.getString(5));
				booklist.add(b);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return booklist;
	}
	@Override
	public List<Book> lowToHigh_Price() {
		List<Book> booklist=new ArrayList<Book>();

		try {
			stat=con1.prepareStatement("select * from Book order by price asc; ");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Book b=new Book();
				b.setBookName(result.getString(2));
				b.setPrice(result.getDouble(7));
				booklist.add(b);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	return booklist;
		
	}
	@Override
	public List<Book> highToLow_Price() {
		List<Book> booklist=new ArrayList<Book>();

		try {
			stat=con1.prepareStatement("select * from Book order by price desc; ");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Book b=new Book();
				b.setBookName(result.getString(2));
				b.setPrice(result.getDouble(7));
				booklist.add(b);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	return booklist;
		
		
	}
	@Override
	public List<Book> bestselling() {
		List<Book> booklist=new ArrayList<Book>();

			try {
				stat=con1.prepareStatement("select * from book order by NoOfCopiesSold desc ");//select max(count) as bestsell,bookname from book
				ResultSet result=stat.executeQuery();
				while(result.next())
				{
					Book b=new Book();
					b.setBookName(result.getString(2));
					b.setNoOfCopiesSold(result.getInt(1));
					booklist.add(b);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
		return booklist;
	}
	
	
	
	

	
}
