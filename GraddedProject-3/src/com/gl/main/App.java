package com.gl.main;
import java.io.IOException;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import com.gl.service.*;

public class App extends Thread{//Extending thread from Thread class
	static Scanner sc;
	UserServiceImpl userservice;
	AdminServiceImpl adminservice;
	public App()//constructor
	{
		System.out.println("********Wellcome to MagicOfBooks*********");
		userservice=new UserServiceImpl();
		adminservice=new AdminServiceImpl();
		sc=new Scanner(System.in);
	}
	public void run()//run method
	{
		//System.out.println("the connection:-"+userservice);
		String loop="y";
		while(loop.equals("y"))
		{
			System.out.println("1.User Menu\n2.Admin Menu");
			int input=sc.nextInt();
			if(input==1)
			{
				userMenu();
			}
			if(input==2)
			{
				adminMenu();
			}
			System.out.println("Do you u want to continue Application(y/n)");
			loop=sc.next();
		}
	}
	//usermenu
	public void userMenu()
	{
		String choice="y";
		while(choice.equals("y"))
		{
			UserServiceImpl userService= new UserServiceImpl();
			System.out.println("1.Register");
			System.out.println("2.login");
			System.out.println("3.selectbookid");
			System.out.println("4.newbooks");
			System.out.println("5.favbooks");
			System.out.println("6.completedbooks");
			System.out.println("0.logout");
			int ch=sc.nextInt();
			switch(ch)
			{
			case 1:
				userService.register();
				break;
			case 2:
				userService.login();
				break;
			case 3:
				try {
					userService.selectbookid();
				} catch (com.gl.exception.NegitiveException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case 4:
				userService.newbooks();
				break;
			case 5:
				userService.favbooks();
				break;
			case 6:
				userService.completedbooks();
				break;
			case 0:
				System.exit(0);
				break;
			default:
				System.out.println("Enter valid choice");
			}
			System.out.println("Do you u want to continue User Services(y/n)");
			choice=sc.next();
		}
		
	}
	//admin menu
	public void adminMenu()
	{
		String choice="y";
		while(choice.equals("y"))
		{
			AdminServiceImpl adminService=new AdminServiceImpl();
			System.out.println("1.Register");
			System.out.println("2.login");
			System.out.println("3.Add Book");
			System.out.println("4.Delete Book");
			System.out.println("5.Update Book");
			System.out.println("6.Display Books");
			System.out.println("7.Toget totalCountOfBooks");
			System.out.println("8.AutoBioGenre Books");
			System.out.println("9.lowToHighPrice order of books");
			System.out.println("10.highToLowPrice order of books");
			System.out.println("11.bestselling of books");
			System.out.println("0.Logout");
			int ch=sc.nextInt();
			switch(ch)//Switch case 
			{
			case 1:
				adminService.register();
				break;
			case 2:
				adminService.login();
				break;
			case 3:
				adminService.addBook();
				break;
			case 4:
				adminService.deleteBook();
				break;
			case 5:
				adminService.updateBook();
				break;
			case 6:
				adminService.displayBook();
				break;
			case 7:
				adminService.totalCountOfBooks();
				break;
			case 8:
				adminService.autoBioGenre();
					break;
			case 9:
				adminService.lowToHighPrice();
				break;
			case 10:														//
				adminService.highToLowPrice();
				break;
			case 11:
				adminService.bestselling();
				break;
			case 0:
				System.exit(0);
				break;
			default:
				System.out.println("Enter valid choice");
			}
			System.out.println("Do you u want to continue Admin Services(y/n)");
			choice=sc.next();
		}
		
	}
	public static void main(String[] args) throws SecurityException, IOException {
		//using loggers
	    boolean append = true;
	    FileHandler handler = new FileHandler("new.log", append);

	    Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	    logger.addHandler(handler);
	     
	    logger.severe("severe message");

	    logger.warning("warning message");

	    logger.info("info message");

	    logger.config("config message");

	    logger.fine("fine message");

	    logger.finer("finer message");

	    logger.finest("finest message");
		App main=new App();
		main.start();// thread start
	}
	
}
