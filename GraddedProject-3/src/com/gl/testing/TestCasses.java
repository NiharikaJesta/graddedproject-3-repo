package com.gl.testing;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.gl.dao.*;
import com.gl.pojo.*;
import com.gl.service.*;

public class TestCasses {
	//declaring DAO classes
	UserDAOImpl usertest;
	AdminDAOImpl admintest;
	@Before
	public void setUp()
	{
		//Initializing DAO classes
		usertest=new UserDAOImpl();
		admintest=new AdminDAOImpl();
	}
	//test cases for user login
	@Test
	public void testLogin() {
		User u=new User();
		u.setUsername("user1");
		u.setPassword("user1");
		u.setUserid(1);
		u.setEmailid("user@1");
		assertTrue(usertest.login(u));
	}
	@After
	public void showMessage()
	{
		System.out.println("user testcases passed");
	}
	
	//test cases for admin login
	@Test
	public void test1() {
		Admin a=new Admin();
		a.setUserid(1);
		a.setUsername("admin1");
		a.setPassword("admin1");
		assertTrue(admintest.login(a));
	}
	@After
	public void showMessage1()
	{
		System.out.println("Admin testcases passed");
	}
}
